package org.sid.cinema.web;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.sid.cinema.dao.FilmRepository;
import org.sid.cinema.dao.TicketRepository;
import org.sid.cinema.entities.Film;
import org.sid.cinema.entities.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.Data;

@RestController
@CrossOrigin("*")
public class CinemaRestController {
	@Autowired
	private FilmRepository filmrepository;
	@Autowired
	private TicketRepository ticketRepository ;
	@GetMapping(path = "/imagefilm/{id}",produces=org.springframework.http.MediaType.IMAGE_JPEG_VALUE)
	public byte[] image(@PathVariable (name="id") long id) throws Exception {
		Film f= filmrepository.findById(id).get(); // je connais le film
		String photoname=f.getPhoto(); //la photo
		File file=new File(System.getProperty("user.home")+"/Cinema/images/"+photoname+".jpg"); //le dossier de user actuelle que demare l'app 
		Path path =Paths.get(file.toURI());
		return Files.readAllBytes(path);
		
		
	}
	
	
	
	
	@PostMapping("/payerTicket")
	@Transactional
	public List<Ticket>payerTickets(@RequestBody TicketForm ticketForm){				 //l objet TicketForm que viendra se format JSON 
		
		
		List<Ticket>listTickets=new ArrayList<Ticket>();									 // 5 pour return la liste des icket vendu
																				//user envoie le ticket que il veux acheté  1
		ticketForm.getTickets().forEach(idTicket->{  							  //2 pour chaque id je charcher a ticket
			Ticket ticket=ticketRepository.findById(idTicket).get();
			ticket.setNomClient(ticketForm.getNomClient());					 // 3 ticket +le nom du client que viens depuis ticket from
			ticket.setReserve(true);
			//ticket.setCodePayement(ticketFrom.getCodePayement());
			ticketRepository.save(ticket);//4
			listTickets.add(ticket); //6
		});
		return listTickets;
		
	}
}

@Data
class TicketForm{
	private String nomClient;
	private int codePayement;
	private List<Long>tickets=new ArrayList<>();
}