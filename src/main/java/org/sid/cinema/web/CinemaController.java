package org.sid.cinema.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.sid.cinema.dao.CategoryRepository;
import org.sid.cinema.dao.CinemaRepository;
import org.sid.cinema.dao.FilmRepository;
import org.sid.cinema.dao.PlaceRepository;
import org.sid.cinema.dao.ProjectionRepository;
import org.sid.cinema.dao.SalleRepository;
import org.sid.cinema.dao.SeanceRepository;
import org.sid.cinema.dao.TicketRepository;
import org.sid.cinema.dao.VilleRepository;
import org.sid.cinema.entities.Categorie;
import org.sid.cinema.entities.Cinema;
import org.sid.cinema.entities.Film;
import org.sid.cinema.entities.Projection;
import org.sid.cinema.entities.Ville;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class CinemaController {

	@Autowired
	private CinemaRepository cinemaRepository;
	@Autowired
	private VilleRepository villeRepository;
	@Autowired

	private SalleRepository salleRepository;
	@Autowired
	private SeanceRepository seanceRepository;
	@Autowired
	private ProjectionRepository projectionRepository;
	@Autowired
	private PlaceRepository placeRepository;
	@Autowired
	private FilmRepository filmRepository;
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private TicketRepository ticketRepository;

	@GetMapping(path = "/test")
	public String index() {
		return "index";
	}

	@GetMapping(path = "/formCinema")
	public String formCinema(Model model) {

		model.addAttribute("mode", "new");
		model.addAttribute("villes", villeRepository.findAll());
		model.addAttribute("cinema", new Cinema());

		return "formCinema";

	}

	@GetMapping(path = "/editcinema")
	public String editcinema(Model model, Long id) {
		Cinema c = cinemaRepository.findById(id).get();/* recuper le cinema e stocké */
		model.addAttribute("villes", villeRepository.findAll());
		model.addAttribute("cinema", c); /* ici */
		model.addAttribute("mode", "edit");
		return "formCinema";

	}

	@GetMapping(path = "/listcinema")
	public String list(Model model, @RequestParam(name = "page", defaultValue = "0") int p,
			@RequestParam(name = "keywoard", defaultValue = "") String kw,
			@RequestParam(name = "size", defaultValue = "6") int s) {
		Page<Cinema> pagecinemas = cinemaRepository.findByNameContains(kw, PageRequest.of(p, s));
		model.addAttribute("cinemas", pagecinemas.getContent());
		model.addAttribute("pages", new int[pagecinemas.getTotalPages()]);
		model.addAttribute("pagecurrent", p);
		model.addAttribute("keywoard", kw);
		return "listcinema";
	}

	@GetMapping(path = "/deleteCinema")
	public String delet(Long id) {
		cinemaRepository.deleteById(id);
		return "redirect:/listcinema";
	}

	@GetMapping(path = "/listville")
	public String listville(Model model, @RequestParam(name = "page", defaultValue = "0") int p,
			@RequestParam(name = "keywoard", defaultValue = "") String kw,
			@RequestParam(name = "size", defaultValue = "6") int s) {
		Page<Ville> pageville = villeRepository.findByNameContains(kw, PageRequest.of(p, s));
		model.addAttribute("villes", pageville.getContent());
		model.addAttribute("pages", new int[pageville.getTotalPages()]);
		model.addAttribute("pagecurrent", p);
		model.addAttribute("keywoard", kw);
		return "listville";
	}

	@GetMapping(path = "/editville")
	public String editville(Model model, Long id) {
		Ville v = villeRepository.findById(id).get();/* recuper le cinema e stocké */
		model.addAttribute("ville", v); /* ici */
		model.addAttribute("mode", "edit");
		return "formVille";

	}

	@GetMapping(path = "/deleteville")
	public String delete(Long id) {
		villeRepository.deleteById(id);
		return "redirect:/listville";
	}

	@PostMapping(path = "/saveCinema")
	public String saveCinema(Model model, @Valid Cinema cinema, BindingResult bindingResult) {
		if (bindingResult.hasErrors())
		return "formCinema";
		cinemaRepository.save(cinema);
		model.addAttribute("cinema", cinema);// pour affchiche sur page confirmation
		return "confirmation";

	}

	@GetMapping(path = "/formVille")
	public String formVille(Model model) {
		model.addAttribute("ville", new Ville());
		model.addAttribute("mode", "new");
		return "formVille";

	}

	@PostMapping(path = "/saveVille")
	public String saveVille(Model model, @Valid Ville ville, BindingResult bindingResult) {
		if (bindingResult.hasErrors())
			return "formVille";
		villeRepository.save(ville);

		model.addAttribute("ville", ville);// pour affchiche sur page confirmation
		return "confirmationVille";

	}

	@GetMapping(path = "/formProjection")
	public String formProjection(Model model) {

		model.addAttribute("mode", "new");
		model.addAttribute("salles", salleRepository.findAll());
		model.addAttribute("filmes", filmRepository.findAll());
		model.addAttribute("seances", seanceRepository.findAll());
		model.addAttribute("projection", new Projection());

		return "formProjection";

	}

	@PostMapping(path = "/saveProj")
	public String saveProj(Model model, @Valid Projection projection, BindingResult bindingResult) {
		if (bindingResult.hasErrors())
			return "formProjection";
		projectionRepository.save(projection);

		model.addAttribute("projection", projection);// pour affchiche sur page confirmation
		return "confirmationProj";

	}

	@GetMapping(path = "/listProjection")
	public String listProjection(Model model, @RequestParam(name = "page", defaultValue = "0") int p,

			@RequestParam(name = "size", defaultValue = "6") int s) {
		Page<Projection> pageprojections = projectionRepository.findAll(PageRequest.of(p, s));
		model.addAttribute("projections", pageprojections.getContent());
		model.addAttribute("pages", new int[pageprojections.getTotalPages()]);
		model.addAttribute("pagecurrent", p);
		model.addAttribute("size", s);
		return "listProjection";
	}

	@GetMapping(path = "/deleteProj")
	public String deleteProj(Long id) {
		projectionRepository.deleteById(id);
		return "redirect:/listProjection";
	}

	@GetMapping(path = "/formFilm")
	public String formFilm(Model model) {
		model.addAttribute("categories", categoryRepository.findAll());
		model.addAttribute("mode", "new");
		model.addAttribute("film", new Film());

		return "formFilm";

	}
	
	@GetMapping(path = "/deleteFilm")
	public String deleteFilm(Long id) {
		filmRepository.deleteById(id);
		return "redirect:/listFilm";
	}


	@PostMapping(path = "/saveFilm")
	public String saveFilm(Model model, @Valid Film film, BindingResult bindingResult) {
		if (bindingResult.hasErrors())
			return "formFilm";
		filmRepository.save(film);

		model.addAttribute("film", film);// pour affchiche sur page confirmation
	
		return "confirmationFilm";

	}
	

	@GetMapping(path = "/listFilm")
	public String listFilm(Model model, @RequestParam(name = "page", defaultValue = "0") int p,

			@RequestParam(name = "size", defaultValue = "6") int s) {
		Page<Film> pagefilms = filmRepository.findAll(PageRequest.of(p, s));
		model.addAttribute("films", pagefilms.getContent());
		model.addAttribute("pages", new int[pagefilms.getTotalPages()]);
		model.addAttribute("pagecurrent", p);
		model.addAttribute("size", s);
		return "listFilm";
	}
	
	
	@GetMapping(path = "/editFilm")
	public String editFilm(Model model, Long id) {
		Film c = filmRepository.findById(id).get();/* recuper le cinema e stocké */
		List<Categorie>categorie = categoryRepository.findAll();
		model.addAttribute("film", c); /* ici */
		model.addAttribute("categories", categorie);
		model.addAttribute("mode", "edit");
		return "formFilm";

	}
	

	
	
	@GetMapping(path = "/getPhoto{id}", produces = org.springframework.http.MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public byte[] getPhoto(Long id) throws FileNotFoundException , IOException {
		Film f = filmRepository.findById(id).get();

		String photoName = f.getPhoto();
		File file = new File(System.getProperty("user.home") + "/cinema/images/" + photoName);
		return org.apache.commons.io.IOUtils.toByteArray(new FileInputStream(file));
	
	}

	public CinemaRepository getCinemaRepository() {
		return cinemaRepository;
	}

	public void setCinemaRepository(CinemaRepository cinemaRepository) {
		this.cinemaRepository = cinemaRepository;
	}

	public VilleRepository getVilleRepository() {
		return villeRepository;
	}

	public void setVilleRepository(VilleRepository villeRepository) {
		this.villeRepository = villeRepository;
	}

	public SalleRepository getSalleRepository() {
		return salleRepository;
	}

	public void setSalleRepository(SalleRepository salleRepository) {
		this.salleRepository = salleRepository;
	}

	public SeanceRepository getSeanceRepository() {
		return seanceRepository;
	}

	public void setSeanceRepository(SeanceRepository seanceRepository) {
		this.seanceRepository = seanceRepository;
	}

	public ProjectionRepository getProjectionRepository() {
		return projectionRepository;
	}

	public void setProjectionRepository(ProjectionRepository projectionRepository) {
		this.projectionRepository = projectionRepository;
	}

	public PlaceRepository getPlaceRepository() {
		return placeRepository;
	}

	public void setPlaceRepository(PlaceRepository placeRepository) {
		this.placeRepository = placeRepository;
	}

	public FilmRepository getFilmRepository() {
		return filmRepository;
	}

	public void setFilmRepository(FilmRepository filmRepository) {
		this.filmRepository = filmRepository;
	}

	public CategoryRepository getCategoryRepository() {
		return categoryRepository;
	}

	public void setCategoryRepository(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}

	public TicketRepository getTicketRepository() {
		return ticketRepository;
	}

	public void setTicketRepository(TicketRepository ticketRepository) {
		this.ticketRepository = ticketRepository;
	}
	
}
