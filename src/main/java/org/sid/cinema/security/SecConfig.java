package org.sid.cinema.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class SecConfig extends WebSecurityConfigurerAdapter{

	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		
		
		PasswordEncoder passwordEncoder=passwordEncoder();
		auth.inMemoryAuthentication().withUser("user1").password(passwordEncoder.encode("123")).roles("user"); 
		auth.inMemoryAuthentication().withUser("user2").password(passwordEncoder.encode("456")).roles("user"); 
		auth.inMemoryAuthentication().withUser("ADMIN").password(passwordEncoder.encode("789")).roles("ADMIN"); 
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
	
		http.formLogin();
		http.exceptionHandling().accessDeniedPage("/notAuthorized"); 
		http.authorizeRequests().antMatchers("/villes**/**","/films**/**","/categories**/**",
				"/cinemas**/**","/places**/**","/projections**/**","/villes**/**","/salles**/**",
				"/seances**/**","/tickets**/**","/imagefilm**/**").permitAll();
		http.authorizeRequests().antMatchers("/save**/**","/delete**/**","/form**/**").hasRole("ADMIN");
		
		http.authorizeRequests().anyRequest().authenticated();	
		
		
	}
	
	
	@Bean       
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
